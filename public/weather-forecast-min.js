const weatherConstantsVariable = {
    "BASE_URL":"http://localhost:3000/"
}

function weatherCreateIframe() {

    let latitude = null;
    let longitude =null;


    const weatherStyle = document.createElement('style');
    weatherStyle.innerHTML = `
    .weather-wrap-element {
        width: 100%;
        height: 100% float:left;
        text-align: center;
        position: fixed;
        top: 35%;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1071;
        padding-bottom: 56.25%;
        padding-top: 1.5625rem;
        transform: translateY(-50%);
        height: 100%;
        padding: 0;
        display: flex;
        align-items: center;
        z-index: 9999;
      }
      .weather-wrap-iframe {

        display:block
        position:fixed;
        margin: 0 auto;
        border: 0;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        opacity: 1;
        z-index: 1072;
      }

      
    `;
    document.head.appendChild(weatherStyle);
    let weatherIframeHeight = null;
    let weatherIframeWidth =  null;
    let weatherDiv = document.createElement("div");
    weatherDiv.setAttribute("class", "weather-wrap-element");
    document.body.appendChild(weatherDiv);
    if (window.devicePixelRatio == 1) {
        weatherIframeHeight = 490;
        weatherIframeWidth = window.innerWidth;
    } else {
        weatherIframeHeight = 460;
        weatherIframeWidth = window.innerWidth;
    }
    let weatherIframe = document.createElement("IFRAME");
    weatherIframe.setAttribute("src", weatherConstantsVariable.BASE_URL);
    weatherIframe.setAttribute("id", "weather-iframe");
    weatherIframe.setAttribute("height", weatherIframeHeight);
    weatherIframe.setAttribute("width", weatherIframeWidth);
    weatherIframe.setAttribute("class", "weather-wrap-iframe");
    weatherDiv.appendChild(weatherIframe);
    if (navigator.geolocation) {
  
        navigator.geolocation.getCurrentPosition((position) => {
    
           latitude = position.coords.latitude;
           longitude = position.coords.longitude;

               weatherForecastSandData({latitude:latitude,longitude:longitude,sendFromParent:true});

      })

    }

   
}

function  weatherForecastSandData(param)
{

 let weatherIframe =  document.getElementById('weather-iframe');

 weatherIframe.contentWindow.postMessage(param,weatherConstantsVariable.BASE_URL);
 weatherIframe.contentWindow.postMessage(" ",weatherConstantsVariable.BASE_URL);

}