import "./App.css";

import Inputs from "./components/Inputs";
import TemperatureAndDetails from "./components/TemperatureAndDetails";
import Forecast from "./components/Forecast";
import getFormattedWeatherData from "./services/weatherService";
import { useEffect, useState } from "react";
import "react-toastify/dist/ReactToastify.css";

function App() {


  const [units, setUnits] = useState("metric");
  const [weather, setWeather] = useState(null);
  let latitude = null;
  let longitude =null;
  if (navigator.geolocation) {

    navigator.geolocation.getCurrentPosition((position) => {

       latitude = position.coords.latitude;
       longitude = position.coords.longitude;

  })

}



const [query, setQuery] = useState(null);



  useEffect(() => {

    const fetchWeather = async () => {



      await getFormattedWeatherData({ ...query, units }).then((data) => {


        setWeather(data);
      });
    };
   
    if(query != null)
    {
      fetchWeather();
    }
    
  }, [query, units]);

  useEffect(() => {
    window.addEventListener("message",(event)=>{
      let obj = event.data;
      if(typeof obj == "object" && "latitude" in obj && "longitude" in obj)
      {
        setQuery({
          lat:obj.latitude,
          lon:obj.longitude,
        });
      }
    });
    if(latitude&&longitude)
    {
      setQuery({
        lat:latitude,
        lon:longitude,
      });

    }
    else{

     setQuery({q: "Kolkata"});
    }
  }, [latitude,longitude]);
  const formatBackground = () => {
    if (!weather) return "from-cyan-700 to-blue-700";
    const threshold = units === "metric" ? 20 : 65;
    //please test this one 
    if (weather.temp <= threshold) return "from-cyan-700 to-blue-700";

    return "from-yellow-700 to-orange-700";
  };

  return (
    <div
      className={`mx-auto max-w-screen-lg		 mt-1 py-2 px-32 bg-gradient-to-br  h-fit shadow-xl shadow-gray-400 ${formatBackground()}`}
    >

      {weather && (
        <div>
          <Inputs setQuery={setQuery} units={units} setUnits={setUnits} weather={weather}/>
          <TemperatureAndDetails weather={weather} />
          <Forecast title="daily forecast" items={weather.daily} />
        </div>
      )}

     
    </div>
  );
}

export default App;