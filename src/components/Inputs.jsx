import React, { useState } from "react";
import { UilSearch, UilLocationPoint } from "@iconscout/react-unicons";
import { formatToLocalTime } from "../services/weatherService";

function Inputs({ setQuery, units, setUnits,weather: { dt, timezone, name, country } }) {
  const [city, setCity] = useState("");

  const handleUnitsChange = (e) => {
    const selectedUnit = e.currentTarget.name;
    if (units !== selectedUnit) setUnits(selectedUnit);
  };

  const handleSearchClick = () => {
    if (city !== "") setQuery({ q: city });
  };

  const handleLocationClick = () => {
    if (navigator.geolocation) {

      navigator.geolocation.getCurrentPosition((position) => {

        let lat = position.coords.latitude;
        let lon = position.coords.longitude;

        setQuery({
          lat,
          lon,
        });
      });
    }
  };

  return (
    <div>
            <div className="flex flex-row justify-start my-6">
        <div className="flex flex-row w-1/4 items-center  justify-start space-x-4" >
        <div className="flex items-left justify-start my-2">
        <p className="text-white text-3xl font-medium">{`${name}, ${country}`}</p>
      </div>
        </div>
    
      <div className="flex flex-row w-2/4 items-center space-x-4" style={{justifyContent: "right"}}>
        <input
          value={city}
          onChange={(e) => setCity(e.currentTarget.value)}
          type="text"
          placeholder="Search for city...."
          className="text-xl font-light p-2  shadow-xl focus:outline-none capitalize placeholder:lowercase"
        />
        <UilSearch
          size={25}
          className="text-white cursor-pointer transition ease-out hover:scale-125"
          onClick={handleSearchClick}
        />
        <UilLocationPoint
          size={25}
          className="text-white cursor-pointer transition ease-out hover:scale-125"
          onClick={handleLocationClick}
        />
      </div>

      <div className="flex flex-row w-1/4 items-center justify-center">
        <button
          name="metric"
          className="text-xl text-white font-light transition ease-out hover:scale-125"
          onClick={handleUnitsChange}
        >
          °C
        </button>
        <p className="text-xl text-white mx-1">|</p>
        <button
          name="imperial"
          className="text-xl text-white font-light transition ease-out hover:scale-125"
          onClick={handleUnitsChange}
        >
          °F
        </button>

   
    </div>
    </div>
      <div className="flex flex-row justify-start my-1">
      <div className="flex flex-row w-2/4 items-center  justify-start space-x-2" >
      <div className="flex items-left justify-start my-1">
        <p className="text-white text-l font-extralight">
          {formatToLocalTime(dt, timezone)}
        </p>
      </div>
      </div>
        </div>
    </div>

  );
}

export default Inputs;